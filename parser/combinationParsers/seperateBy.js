import Parser from '../Parser.js';
import {updateErrorState, updateParserResult} from '../utils.js';

const seperateBy = (seperateParser) =>
  (valueParser) => new Parser((parserstate) => {
    const results = [];
    let nextState = parserstate;
    while (true) {
      console.log(nextState);
      const updatedState = valueParser.parserStateTransformerFn(nextState);
      if (updatedState.isError) break;
      results.push(updatedState.result);
      nextState = updatedState;
      console.log(nextState);
      const seperatorState = seperateParser.parserStateTransformerFn(nextState);
      if (seperatorState.isError) break;
      nextState = seperatorState;
    }

    return updateParserResult(nextState, results);
  });

const seperateByOne = (seperateParser) =>
  (valueParser) => new Parser((Parserstate) => {
    const results = [];
    let nextState = Parserstate;
    while (true) {
      const updatedState = valueParser.run(nextState);
      if (updatedState, isError) break;
      results.push(updatedState);
      nextState = updatedState;

      const seperatorState = seperateParser.run(nextState);
      if (seperatorState.isError) break;
      nextState = seperatorState;
    }

    return updateErrorState(
        Parserstate,
        `Unable to capture error @ index ${Parserstate.index}`);
  });

export {seperateBy, seperateByOne};
