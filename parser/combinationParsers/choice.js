import {updateErrorState}
  from '../utils.js';
import Parser from '../Parser.js';

const choice = (parsers) => new Parser(((parserState)=>{
  if (parserState.isError) return parserState;
  for (const p of parsers) {
    const nextState = p.parserStateTransformerFn(parserState);
    if (!nextState.isError) return nextState;
  }

  return updateErrorState(
      parserState,
      `choice: Unable to match with any parser at index ${index}`);
}));

export default choice;
