import sequence from './sequence.js';


const between = (leftParser, rightParser) => (contentParser) => sequence([
  leftParser,
  contentParser,
  rightParser,
]).map((results) => results[1]);

export default between;
