import {updateParserResult}
  from '../utils.js';
import Parser from '../Parser.js';


const sequence = (parsers) => new Parser((parserState) => {
  if (parserState.isError) return parserState;

  const results = [];
  let nextState = parserState;
  for (const p of parsers) {
    nextState = p.parserStateTransformerFn(nextState);
    results.push(nextState.result);
  }
  return updateParserResult(nextState, results);
});

export default sequence;
