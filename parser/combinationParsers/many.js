import {updateParserResult}
  from '../utils.js';
import Parser from '../Parser.js';


const many = (parser) => new Parser((parserState) => {
  const results = [];
  let done = false;
  let nextState = parserState;
  while (!done) {
    const testState = parser.parserStateTransformerFn(nextState);
    if (!testState.isError) {
      results.push(testState.result);
      nextState = testState;
    } else done = true;
  }

  return updateParserResult(nextState, results);
});

export default many;
