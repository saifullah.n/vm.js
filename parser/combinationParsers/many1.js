import {updateErrorState, updateParserResult}
  from '../utils.js';
import Parser from '../Parser.js';


const many1 = (parser) => new Parser((parserState) => {
  const results = [];
  let done = false;
  let nextState = parserState;
  while (!done) {
    nextState = parser.parserStateTransformerFn(nextState);
    if (!nextState.isError) {
      results.push(nextState.result);
    } else done = true;
  }
  if ( ! results.length) {
    return updateErrorState(
        parserState,
        `many1: No Macthes found for input at index ${parserState.index}`);
  }
  return updateParserResult(nextState, results);
});

export default many1;
