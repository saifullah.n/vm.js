import {updateErrorState, updateParserResult}
  from './utils.js';

class Parser {
  constructor(parserStateTransformerFn) {
    this.parserStateTransformerFn = parserStateTransformerFn;
  }

  run(targetString) {
    const initialState = {
      targetString,
      result: null,
      index: 0,
      error: null,
      isError: false};
    return this.parserStateTransformerFn(initialState);
  };
  map(fn) {
    return new Parser((parserState)=>{
      const nextState = this.parserStateTransformerFn(parserState);
      if (nextState.isError) return nextState;
      return updateParserResult(nextState, fn(nextState.result));
    });
  }

  errorMap(fn) {
    return new Parser((parserState)=>{
      const nextState = this.parserStateTransformerFn(parserState);
      if (!nextState.isError) return nextState;
      return updateErrorState(nextState,
          fn(nextState.error, nextState.index));
    });
  }

  chain(fn) {
    return new Parser((parserState)=>{
      const nextState = this.parserStateTransformerFn(parserState);
      if (nextState.isError) return nextState;
      const nextParser = fn(nextState.result);
      return nextParser.parserStateTransformerFn(nextState);
    });
  }
}

export default Parser;
