const updateParserState = (state, index, result)=>({
  ...state,
  index,
  result,
});

const updateParserResult = (state, result)=>({
  ...state,
  result,
});


const updateErrorState = (state, errorMsg)=>({
  ...state,
  isError: true,
  error: errorMsg,
  result: null,
  index: 0,
});

const lazyEvalFn = (parserThunk) => new Parser((parserState)=>{
  const parser = parserThunk();
  return parser.parserStateTransformerFn(parserState);
});

export {updateErrorState, updateParserResult, updateParserState, lazyEvalFn};
