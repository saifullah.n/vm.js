import {updateErrorState,
  updateParserState}
  from '../utils.js';
import Parser from '../Parser.js';

const str = (s) => new Parser((parserState) => {
  const {index, targetString} = parserState;
  const slicedTarget = targetString.slice(index);

  if ( !slicedTarget.length) {
    return updateErrorState(parserState, `str: Unexpected EOL`);
  }

  if (parserState.isError) return parserState;

  if (slicedTarget.startsWith(s)) {
    return updateParserState(parserState, index+s.length, s);
  };

  return updateErrorState(parserState,
      `Expected ${s} but got ${targetString.slice(index, index + 10)}`);


  // eslint-disable-next-line max-len
  throw new Error(`Expected ${s} but got ${targetString.slice(index, index + 10)}`);
});

export default str;
