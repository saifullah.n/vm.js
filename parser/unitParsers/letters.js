import {updateErrorState,
  updateParserState}
  from '../utils.js';
import Parser from '../Parser.js';


const letterRegex = /^[A-Za-z]+/;

const letters = new Parser((parserState) => {
  const {index, targetString} = parserState;
  const slicedTarget = targetString.slice(index);

  if ( !slicedTarget.length) {
    return updateErrorState(parserState, `letter: Unexpected EOL`);
  }

  if (parserState.isError) return parserState;

  const regexMatch = slicedTarget.match(letterRegex);

  if (regexMatch ) {
    return updateParserState(parserState,
        index+ regexMatch[0].length,
        regexMatch[0]);
  };

  return updateErrorState(
      parserState,
      `letter: Couldn't match letters at index : ${index}`);


  // eslint-disable-next-line max-len
  throw new Error(`Expected ${s} but got ${targetString.slice(index, index + 10)}`);
});

export default letters;
